function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive 
}

function updateMess(msg, color) {
    document.getElementById("message").innerHTML = msg;
    document.getElementById("message").style.backgroundColor = color;

}

function reloadpage(msg) {
    alert(msg);
    location.reload();
}

var myvalue = getRandomIntInclusive(0, 1000);
myvalue = 10
let button = document.getElementById('guessButton');
var count = 10;
var past = "";
var pastCount = 0;
button.addEventListener('click', function () {
    if (count > 0) {
        var userGuess = parseInt(document.getElementById("number").value);
        past = `<div style="font-size: ${parseInt(16 + pastCount * 1.5)}px; ">${userGuess}</div>` + past
        pastCount += 1;
        document.getElementById("past").innerHTML = past;
        if (userGuess == myvalue) {
            updateMess("Chuc mung ban doan dung roi!", "green")
            setTimeout(reloadpage, 50, "Thang roi")

        } else {
            count--;
            updateMess("Doan sai roi con " + count.toString() + " lan nhap!", "red");
            if (count == 0) {
                setTimeout(reloadpage, 10, "Thua roi")

            }
            if (userGuess > myvalue) {
                document.getElementById("message").innerHTML += " Giam nho lai";
            } else {
                document.getElementById("message").innerHTML += " Tang len xiu";

            }
        }

    }
});